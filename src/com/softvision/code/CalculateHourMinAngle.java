package com.softvision.code;

/*
 A clock is a circle so therefore is is 360 degrees.

 There are 60 min ticks or positions around the face of the clock.
 - Divide 360 by 60 equals 6 degrees per minute ticks

 There are 12 hour ticks or positions around the face of the clock.
 - Divide 360 by 12 equals 30 degrees per hour

 So, for every 360 rotation of the minute hand, the hour hand will move
 30 degrees.

 Now, once a min, the hour hand will move slightly
 - Divide 30 degrees by 60 minutes equals 0.5 degrees per minute movement

 */
public class CalculateHourMinAngle {

	private double calculate(String str) {
		String[] vals = str.split(":");
		double hour = Double.parseDouble(vals[0]);
		double minute = Double.parseDouble(vals[1]);
		return  calculate(hour, minute);
	}

	private double calculate(double hour, double minute) {
		double a, b;


		if(hour >= 12) {
			hour = hour - 12;
		}

		double hourAngle = (hour * 30) + (minute * 0.5);
		double minAngle = 6 * minute;

		if(hourAngle < minAngle) { a = minAngle; b = hourAngle; }
		else { a = hourAngle; b = minAngle; }

		System.out.println("The hour angle is " + hourAngle + " and the minute angle is " + minAngle);

		double result = a - b;
		if(result < 0) result = result + 360;
		if(result > 180) result = result - 180;

		return  result;
	}

	public static void main(String[] args) {
		CalculateHourMinAngle chma = new CalculateHourMinAngle();
		String time = "2:12";
		double angle = chma.calculate(time);
		System.out.println("The angle between the hour and minute hand for " + time + " is " + angle);
	}
}
