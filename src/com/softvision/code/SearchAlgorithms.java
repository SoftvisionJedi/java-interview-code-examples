package com.softvision.code;

import com.softvision.helpers.ArrayTools;

public class SearchAlgorithms {
	public static void main(String[] args) {
		String str = "Now let me tell you a story about a man name Jed. The poor mountaineer kept his family barely fed. Then one day, he was shooting at some food";
		SearchAlgorithms sa = new SearchAlgorithms();
		ArrayTools at = new ArrayTools();

		char val = sa.firstNonRepeatedCharacter(str);
		System.out.println("The first non-repeating character is " + val);
	}

	private char firstNonRepeatedCharacter(String val) {
		String str = val.toLowerCase();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (str.indexOf(c) == i && str.indexOf(c, i + 1) == -1) {
				return c;
			}
		}
		return ' ';
	}
}
