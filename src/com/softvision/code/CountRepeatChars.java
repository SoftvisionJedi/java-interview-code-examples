package com.softvision.code;

import java.util.HashMap;
import java.util.Map;

public class CountRepeatChars {

	private String exec(String str) {
		StringBuilder sb = new StringBuilder();
		Map<Character, Integer> map = new HashMap<>();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (map.containsKey(c)) {
				int cnt = map.get(c);
				map.put(c, ++cnt);
			} else {
				map.put(c, 1);
			}
		}

		map.forEach((key, value) -> sb.append(key).append(value));
		return sb.toString();
	}

	private String getOccurringChar(String str)
	{
		StringBuilder sb = new StringBuilder();
		int MAX_CHAR = 256;
		int[] count = new int[MAX_CHAR];
		int len = str.length();

		for (int i = 0; i < len; i++) { count[str.charAt(i)]++; }

		char[] ch = new char[str.length()];
		for (int i = 0; i < len; i++) {
			ch[i] = str.charAt(i);
			int find = 0;
			for (int j = 0; j <= i; j++) {
				if (str.charAt(i) == ch[j]) find++;
			}

			if (find == 1)
				sb.append(str.charAt(i)).append(count[str.charAt(i)]);
		}
		return  sb.toString();
	}

	public static void main(String[] args) {
		CountRepeatChars crc = new CountRepeatChars();
		String str = "aaabbcccccddeffggghhhh";
		String val1 = crc.exec(str);
		System.out.println("The result for " + str + " is " + val1);

		String val2 = crc.getOccurringChar(str);
		System.out.println("The result for " + str + " is " + val2);
	}
}
