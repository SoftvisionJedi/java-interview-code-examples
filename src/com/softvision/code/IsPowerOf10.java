package com.softvision.code;

public class IsPowerOf10 {

	private boolean isPowerOfTen(int input) {
		while (input > 9 && input % 10 == 0) {
			input /= 10;
		}
		return input == 1;
	}

	public static void main(String[] args) {

		IsPowerOf10 s = new IsPowerOf10();
		int val = 100;
		boolean b = s.isPowerOfTen(val);
		System.out.println("Is "+ val + " a power of 10: " + b);

	}
}
