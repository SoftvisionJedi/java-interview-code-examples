package com.softvision.examples;

import com.softvision.examples.RunnableWorker;
import com.softvision.examples.ThreadWorker;

import java.util.*;

public class Main {
	private static final int[] aNums1 = {2, 5, 7, 34, 22, 68, 11, 65, 3, 9, 4, 54, 88, 89, 101, 48, 41, 36, 121};
	private static final int[] aNums2 = {17, 29, 31, 13, 14, 18, 21, 1, 6, 55, 20, 84, 86, 0, 98, 8, 44, 87, 63, 39, 77};

	public static void main(String[] args) {

		System.out.println(Arrays.toString(sortNumbers(aNums1)));

		int[] val = findMedianSortedArrays(aNums1, aNums2);

		System.out.print("Final Result: ");
		for (int i : val)
			System.out.print(i + "  ");

		System.out.println();

		boolean test = isPalindrome("Racecar");
		System.out.println("The word is " + Boolean.toString(test));


		System.out.println("The number is " + Boolean.toString(isNumPalindrome5(121)));


		int i = lengthOfLongestSubstring2("asdsrgfhgdwedsarujkhgfyttfdddsstryytrgbdgjikuyqwertfgrew");
		System.out.println("Longest Substring is " + i);

		usingSetExample();

		FindLargestNum();

		System.out.println(Boolean.toString(SimplePalindrome("Racecar")));

		WordSearch();

		int n = 9;
		System.out.println(fib1(n));
		System.out.println(fib2(n));
		System.out.println(fib3(n));

		ThreadingExample();
	}

	public static void ThreadingExample() {
		Thread t1 = new Thread(new RunnableWorker(), "t1");
		Thread t2 = new Thread(new RunnableWorker(), "t2");
		System.out.println("Starting Runnable threads");
		t1.start();
		t2.start();
		System.out.println("Runnable Threads has been started");
		Thread t3 = new ThreadWorker("t3");
		Thread t4 = new ThreadWorker("t4");
		System.out.println("Starting ThreadWorkers");
		t3.start();
		t4.start();
		System.out.println("ThreadWorker has been started");
	}

	private static int[] sortNumbers(int[] numbers) {
		int temp = 0;
		for (int i = 0; i < numbers.length - 1; i++) {
			for (int j = i + 1; j < numbers.length; j++) {
				if (numbers[i] > numbers[j]) {
					temp = numbers[i];
					numbers[i] = numbers[j];
					numbers[j] = temp;
				}
			}
		}
		return numbers;
	}

	private static int[] findMedianSortedArrays(int[] nums1, int[] nums2) {
		int[] nums = new int[(nums1.length + nums2.length)];
		System.arraycopy(nums1, 0, nums, 0, nums1.length);
		System.arraycopy(nums2, 0, nums, nums1.length, nums2.length);

		Arrays.sort(nums);

		for (int i : nums)
			System.out.print(i + "  ");

		System.out.println();

		if (nums.length % 2 == 0) {
			int idx = (nums.length / 2);
			return Arrays.copyOfRange(nums, idx, (idx + 2));
		} else {
			int idx = (((nums.length - 1) / 2) + 1);
			return Arrays.copyOfRange(nums, idx, (idx + 1));
		}
	}

	private static boolean isPalindrome(String str) {
		return str.equalsIgnoreCase(new StringBuilder(str).reverse().toString());
	}

	private static boolean isNumPalindrome1(int x) {
		String num = Integer.toString(x);
		return num.equals(new StringBuilder(num).reverse().toString());
	}

	private static boolean isNumPalindrome2(int n) {
		if (n < 0 || n == 10) return false;

		int len = (int) (Math.log10(n) - 1);

		int max = 10;

		for (int i = 0; i < len; i++) {
			max *= 10;
		}

		while (len > 0) {
			len--;
			int a = n / max;
			int b = n % 10;

			System.out.println(Integer.toString(n) + " --> " + Integer.toString(a) + " | " + Integer.toString(b));

			if (a != b) return false;

			n = n % max;
			n = n / 10;
			max = max / 10 / 10;

			if (n == 0) break;
		}

		return true;
	}

	private static boolean isNumPalindrome3(int n) {
		int r, sum = 0, temp;

		while (n > 0) {
			temp = n;
			r = n % 10;
			sum = (sum * 10) + r;
			n = n / 10;

			System.out.println(Integer.toString(n) + " --> " + Integer.toString(temp) + " | " + Integer.toString(sum));

			//if(temp != sum) return false;
		}

		return true;
	}

	private static boolean isNumPalindrome4(int n) {
		if (n < 0) return false;

		int a = n;
		int b = 0, c;

		while (a != 0) {
			c = a % 10;
			b = b * 10 + c;
			a = a / 10;
		}
		return n == b;
	}

	// BRAD'S ATTEMPTED - FAILED
	private static boolean isNumPalindrome5(int n) {
		if (n < 0) return false;
		int v = (int) Math.sqrt((double) n);
		int r = (v - 1) * v;

		System.out.println("v = " + Integer.toString(v) + " | r = " + Integer.toString(r));
		return n == r;
	}

	private static int lengthOfLongestSubstring1(String s) {
		int len = 1;
		char[] cmp = s.toCharArray();
		StringBuilder sb = new StringBuilder();

		sb.append(cmp[0]);

		for (int i = 0; i < s.length(); i++) {
			if (i < (s.length() - 1)) {
				if (cmp[(i + 1)] == cmp[i]) {
					len = 1;

				} else {
					len++;
				}
			}
		}
		return len;
	}

	private static int lengthOfLongestSubstring2(String s) {
		if (s == null)
			return 0;
		boolean[] flag = new boolean[256];

		int result = 0;
		int start = 0;
		char[] arr = s.toCharArray();

		for (int i = 0; i < arr.length; i++) {
			char current = arr[i];
			if (flag[current]) {
				result = Math.max(result, i - start);
				// the loop update the new start point
				// and reset flag array
				// for example, abccab, when it comes to 2nd c,
				// it update start from 0 to 3, reset flag for a,b
				for (int k = start; k < i; k++) {
					if (arr[k] == current) {
						start = k + 1;
						break;
					}
					flag[arr[k]] = false;
				}
			} else {
				flag[current] = true;
			}
		}

		result = Math.max(arr.length - start, result);

		return result;
	}

	/*

	The Fibonacci numbers are the numbers in the following integer sequence.
	0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ……..

	In mathematical terms, the sequence Fn of Fibonacci numbers is defined by the recurrence relation
	Fn = Fn-1 + Fn-2

	 */
	//Fibonacci Series using Recursion
	static int fib1(int n) {
		if (n <= 1)
			return n;
		return fib1(n - 1) + fib1(n - 2);
	}

	// Fibonacci Series using Dynamic Programming
	static int fib2(int n) {
		/* Declare an array to store Fibonacci numbers. */
		int f[] = new int[n + 2]; // 1 extra to handle case, n = 0
		int i;

		/* 0th and 1st number of the series are 0 and 1*/
		f[0] = 0;
		f[1] = 1;

		for (i = 2; i <= n; i++) {
       /* Add the previous 2 numbers in the series
         and store it */
			f[i] = f[i - 1] + f[i - 2];
		}

		return f[n];
	}

	// Java program for Fibonacci Series using Space Optimized Method
	static int fib3(int n) {
		int a = 0, b = 1, c = 0;

		// To return the first Fibonacci number
		if (n == 0) return a;

		for (int i = 2; i <= n; i++) {
			c = a + b;
			a = b;
			b = c;
		}

		return b;
	}


	private static void usingSetExample() {
		List<Integer> listNumbers = Arrays.asList(3, 9, 1, 4, 7, 2, 5, 3, 8, 9, 1, 3, 8, 6);
		System.out.println(listNumbers);
		Set<Integer> uniqueNumbers = new HashSet<>(listNumbers);
		System.out.println(uniqueNumbers);
	}

	private static void FindLargestNum() {
		int[] myNums = {45, 76442, 56, 5431, 797, 43, 90, 654, 3636, 32, 89, 78, 708796, 4768, 905673, 95, 32235, 7, 43547, 32, 485534, 3262, 47329, 483, 25436, 590, 3215, 845, 1, 46, 25254, 6};
		int[] maxVals = {0, 0, 0, 0, 0};

		for (int val : myNums) {
			if (val > maxVals[0]) {
				maxVals[4] = maxVals[3];
				maxVals[3] = maxVals[2];
				maxVals[2] = maxVals[1];
				maxVals[1] = maxVals[0];
				maxVals[0] = val;
			} else if (val > maxVals[1]) {
				maxVals[4] = maxVals[3];
				maxVals[3] = maxVals[2];
				maxVals[2] = maxVals[1];
				maxVals[1] = val;
			} else if (val > maxVals[2]) {
				maxVals[4] = maxVals[3];
				maxVals[3] = maxVals[2];
				maxVals[2] = val;
			} else if (val > maxVals[3]) {
				maxVals[4] = maxVals[3];
				maxVals[3] = val;
			} else if (val > maxVals[4]) {
				maxVals[4] = val;
			}
		}

		myNums = SimpleSort2(myNums);
		System.out.println(Arrays.toString(myNums));
		System.out.println(Arrays.toString(maxVals));
	}

	private static int[] SimpleSort(int[] vals) {
		int val = 0;

		System.out.println("-----------------------------------------------");

		for (int a = (vals.length - 1); a >= 0; a--) {
			for (int b = a - 1; b >= 0; b--) {
				if (vals[a] > vals[b]) {
					val = vals[b];
					vals[b] = vals[a];
					vals[a] = val;
				}
			}
		}
		return vals;
	}

	private static boolean SimplePalindrome(String word) {
		System.out.println("SimplePalindrome-----------------------------------------------");

		char[] letters1 = word.toCharArray();
		char[] letters2 = new char[letters1.length];

		for (int a = 0; a < letters1.length; a++) {
			letters2[(letters1.length - 1) - a] = letters1[a];
		}

		String word2 = String.copyValueOf(letters2);
		return word.equalsIgnoreCase(word2);
	}

	private static void WordSearch() {
		String paragraph = "Is it possible that the greatest coach in sports today has never trained a two-legged athlete?\n" +
			"\n" +
			"Don't laugh. Horse racing's Bob Baffert could make that a legitimate debate on Saturday if he wins a historic second Triple Crown when Justify takes aim at the Belmont Stakes.\n" +
			"\n" +
			"The ivory-haired trainer who, by all accounts, has genius-level horse sense, also has plenty more in common with Bill Belichick than just their initials.\n" +
			"\n" +
			"Baffert-trained horses have dominated Triple Crown races for more than two decades, since he won his first two in 1997. That's even longer than Belichick's New England Patriots have ruled the NFL postseason. It's also longer than Gregg Popovich's Spurs have been perennial NBA contenders or Nick Saban's Alabama teams have been college football juggernauts.";

		String[] words = paragraph.split("\\s+");
		Set<String> wordSet = new HashSet<>();

		for (int a = 0; a < words.length; a++) {

			words[a] = words[a].replace("([^A-Za-z])\\w+", "");
		}

		for (String word : words) {
			wordSet.add(word);
		}

		System.out.println(Arrays.toString(wordSet.toArray()));
	}

	private static int[] SimpleSort2(int[] vals) {
		int val;

		System.out.println("V2-----------------------------------------------");
		for (int a = (vals.length - 1); a >= 0; a--) {
			for (int b = (a - 1); b >= 0; b--) {
				if (vals[a] > vals[b]) {
					val = vals[a];
					vals[a] = vals[b];
					vals[b] = val;
				}
			}
		}
		return vals;
	}


	private static void bubbleSort(int ar[]) {
		for (int i = (ar.length - 1); i >= 0; i--) {
			for (int j = 1; j <= i; j++) {
				if (ar[j - 1] > ar[j]) {
					int temp = ar[j - 1];
					ar[j - 1] = ar[j];
					ar[j] = temp;
				}
			}
		}
	}

	private static void selectionSort(int[] ar) {
		for (int i = 0; i < ar.length - 1; i++) {
			int min = i;

			for (int j = i + 1; j < ar.length; j++)
				if (ar[j] < ar[min]) min = j;

			int temp = ar[i];
			ar[i] = ar[min];
			ar[min] = temp;
		}
	}

	private static void insertionSort(int[] ar) {
		for (int i = 1; i < ar.length; i++) {
			int index = ar[i];
			int j = i;
			while (j > 0 && ar[j - 1] > index) {
				ar[j] = ar[j - 1];
				j--;
			}
			ar[j] = index;
		}
	}


	void merge(int arr[], int l, int m, int r) {
		// Find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;

		/* Create temp arrays */
		int L[] = new int[n1];
		int R[] = new int[n2];

		/*Copy data to temp arrays*/
		for (int i = 0; i < n1; ++i)
			L[i] = arr[l + i];
		for (int j = 0; j < n2; ++j)
			R[j] = arr[m + 1 + j];


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays
		int i = 0, j = 0;

		// Initial index of merged subarry array
		int k = l;
		while (i < n1 && j < n2) {
			if (L[i] <= R[j]) {
				arr[k] = L[i];
				i++;
			} else {
				arr[k] = R[j];
				j++;
			}
			k++;
		}

		/* Copy remaining elements of L[] if any */
		while (i < n1) {
			arr[k] = L[i];
			i++;
			k++;
		}

		/* Copy remaining elements of R[] if any */
		while (j < n2) {
			arr[k] = R[j];
			j++;
			k++;
		}
	}

	// Main function that sorts arr[l..r] using
	// merge()
	void sort(int arr[], int l, int r) {
		if (l < r) {
			// Find the middle point
			int m = (l + r) / 2;

			// Sort first and second halves
			sort(arr, l, m);
			sort(arr, m + 1, r);

			// Merge the sorted halves
			merge(arr, l, m, r);
		}
	}

	private static void mergeSort(int[] ar) {
		//T(n) = 2*T(n/2) + n - 1
	}
}
