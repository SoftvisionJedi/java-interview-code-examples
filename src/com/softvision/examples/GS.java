package com.softvision.examples;

import java.util.*;

public class GS {
	
	public static void main(String[] args) {
		int val = 500;
		int[] array = new int[]{2, 12333, 21, 22, 2, 1, 12, 13, 1, 10, 34, 1, 2, 353, 23, 56, 2, 54, 332, 32, 64, 2314};
		String str = "Now let me tell you a story about a man name Jed. The poor mountaineer kept his family barely fed. Then one day, he was shooting at some food";
		
		System.out.println(isPowerOfTen(val));
		
		System.out.println(isPowerOf(2, 128));
		
		System.out.println(findSecondSmallestNum(array));

		System.out.println(firstNonRepeatedCharacter(str));
	}
	
	private static boolean isPowerOfTen(int num) {
		while (num > 9 && num % 10 == 0) { num /= 10; }
		return num == 1;
	}
	
	private static boolean isPowerOf(int x, int y) {
		// The only power of 1 is 1 itself
		if (x == 1) { return (y == 1); }
		
		// Repeatedly compute power of x
		int pow = 1;
		while (pow < y) { pow = pow * x; }
		return (pow == y);
	}
	
	private static int findSecondSmallestNum(int[] arr) {
		if(arr.length < 2)  { return -1; }
		
		int min1 = Integer.MAX_VALUE;
		int min2 = Integer.MAX_VALUE;
		
		for (int value : arr) {
			if (value < min1) {
				min2 = min1;
				min1 = value;
			} else if (value < min2 && value != min1) {
				min2 = value;
			}
		}
		
		return  min2;
	}
	
	private static char firstNonRepeatedCharacter(String val) {
	String str = val.toLowerCase();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (str.indexOf(c) == i && str.indexOf(c, i + 1) == -1) {
				return c;
			}
		}
		return ' ';
	}
}
